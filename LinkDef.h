#ifdef TEMPERATURE_H
    #pragma link C++ struct Temperature+;
#endif

#ifdef ADC64_EVENT_H
    #pragma link C++ struct ADC64Frame+;
    #pragma link C++ struct ADC64Event+;
#endif