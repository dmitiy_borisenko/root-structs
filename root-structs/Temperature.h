#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <TNamed.h>

struct Temperature : public TNamed
{
    UShort_t t_adc;
    Double_t t_cdeg;

    Temperature();

ClassDef(Temperature, 1)
};

#endif