QT -= gui
TEMPLATE = lib
TARGET = RootStructs

CONFIG += cern-root
include(config.pri)

HEADERS +=  $$PWD/root-structs/Temperature.h    \
            $$PWD/root-structs/ADC64Event.h
SOURCES +=  $$PWD/root-structs/Temperature.cc   \
            $$PWD/root-structs/ADC64Event.cc

CLING_HEADERS = $$HEADERS $$PWD/LinkDef.h
rootcling.target = $${TARGET}.cxx
rootcling.commands = rootcling -f $$rootcling.target -c $$CLING_HEADERS
rootcling.depends = $$CLING_HEADERS 
QMAKE_EXTRA_TARGETS += rootcling
SOURCES += $$rootcling.target